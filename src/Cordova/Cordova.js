import {GoogleAnalytics} from "./Plugins/GoogleAnalytics";
import {NetworkInformation} from "./Plugins/NetworkInformation";

export const LktCordova = {
    GoogleAnalytics: GoogleAnalytics,
    NetworkInformation: NetworkInformation,
};