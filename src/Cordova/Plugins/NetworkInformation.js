export const NetworkInformation = {

    /**
     *
     * @returns {boolean}
     */
    isUnknown() {
        return navigator.connection.type === Connection.UNKNOWN;
    },

    /**
     *
     * @returns {boolean}
     */
    isOffline() {
        return navigator.connection.type === Connection.NONE;
    },

    /**
     *
     * @returns {boolean}
     */
    isOnline() {
        return false === this.isOffline()
            && false === this.isUnknown();
    },

    /**
     *
     * @returns {boolean}
     */
    isMobileConnection() {
        switch (navigator.connection.type) {
            case Connection.CELL_4G:
            case Connection.CELL_3G:
            case Connection.CELL_2G:
            case Connection.CELL:
                return false === this.isRegularConnection();
            default:
                return false;

        }
    },

    /**
     *
     * @returns {boolean}
     */
    isRegularConnection() {
        switch (navigator.connection.type) {
            case Connection.ETHERNET:
            case Connection.WIFI:
                return true;
            default:
                return false;

        }
    }
};