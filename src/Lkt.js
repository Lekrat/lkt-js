import {Helper} from "./Vanilla/Helper";
import {Navigator} from "./Vanilla/Navigator";
import {SafeType} from "./Vanilla/SafeType";
import {Session} from "./Vanilla/Session/Session";
import {Validation} from "./Vanilla/Form/Validation";
import {Validator} from "./Vanilla/Form/Validator";
import {Document} from "./Vanilla/Document/Document";

export const Lkt = {
    Helper: Helper,
    Navigator: Navigator,
    SafeType: SafeType,
    Session: Session,
    Form: {
        Validation: Validation,
        Validator: Validator
    },
    Document: Document
};