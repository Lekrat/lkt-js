import {Lkt} from "./Lkt";
import {LktVue} from "./Vue/Vue";
import {LktCordova} from "./Cordova/Cordova";
Lkt.Vue = LktVue;
Lkt.Cordova = LktCordova;
export {Lkt};