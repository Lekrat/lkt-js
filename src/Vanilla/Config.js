export class Config {

    static addValidation(name, fn) {
        Config.validations[name] = fn;
    }

    static getValidations() {
        return Config.validations;
    }
}

Config.validations = {};

// export class Config;
