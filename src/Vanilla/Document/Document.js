import {Helper} from "./Helper";
import {StickyElement} from "./StickyElement";

export const Document = {
    Helper,
    StickyElement,
};