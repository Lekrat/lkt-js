export const Helper = {

    count(t, e) {
        let r, n = 0;
        if (null === t || "undefined" == typeof t) {
            return 0;
        }
        if (t.constructor !== Array && t.constructor !== Object) {
            return 1;
        }
        "COUNT_RECURSIVE" === e && (e = 1), 1 != e && (e = 0);
        for (r in t) t.hasOwnProperty(r) && (n++, 1 != e || !t[r] || t[r].constructor !== Array && t[r].constructor !== Object || (n += this.count(t[r], 1)));
        return n
    },

    explode(t, e, r) {
        if (arguments.length < 2 || "undefined" == typeof t || "undefined" == typeof e) {
            return null;
        }
        if ("" === t || t === !1 || null === t) {
            return !1;
        }
        if ("function" == typeof t || "object" == typeof t || "function" == typeof e || "object" == typeof e) {
            return {
                0: ""
            };
        }
        t === !0 && (t = "1"), t += "", e += "";
        let n = e.split(t);
        return "undefined" == typeof r ? n : (0 === r && (r = 1), r > 0 ? r >= n.length ? n : n.slice(0, r - 1).concat([n.slice(r - 1).join(t)]) : -r >= n.length ? [] : (n.splice(n.length + r), n))
    },


    /**
     * @param str_json
     */
    jsonDecode(str_json) {
        let json = window.JSON;

        if ("object" == typeof json && "function" == typeof json.parse) {
            try {
                return json.parse(str_json)
            } catch (err) {
                if (!(err instanceof SyntaxError)) {
                    throw new Error("Unexpected error type in json_decode()");
                }

                return this.php_js = this.php_js || {}, this.php_js.last_error_json = 4, null
            }
        }

        let cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            j, text = str_json;

        return cx.lastIndex = 0, cx.test(text) && (text = text.replace(cx, function (t) {
            return "\\u" + ("0000" + t.charCodeAt(0).toString(16)).slice(-4)
        })), /^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ? j = eval("(" + text + ")") : (this.php_js = this.php_js || {}, this.php_js.last_error_json = 4, null)
    },

    jsonToArray(t) {
        return Object.keys(t).map(function (e) {
            return t[e]
        })
    },

    numberFormat(t, e, r, n) {
        t = (t + "").replace(/[^0-9+\-Ee.]/g, "");
        let o = isFinite(+t) ? +t : 0,
            i = isFinite(+e) ? Math.abs(e) : 0,
            u = "undefined" === typeof n ? "," : n,
            a = "undefined" === typeof r ? "." : r,
            s = "",
            f = function (t, e) {
                let r = Math.pow(10, e);
                return "" + (Math.round(t * r) / r).toFixed(e)
            };
        return s = (i ? f(o, i) : "" + Math.round(o)).split("."), s[0].length > 3 && (s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, u)), (s[1] || "").length < i && (s[1] = s[1] || "", s[1] += new Array(i - s[1].length + 1).join("0")), s.join(a)
    },

    strlen(t) {
        let e = t + "",
            r = 0,
            n = "",
            o = 0;

        let i = function (t, e) {
            let r = t.charCodeAt(e),
                n = "",
                o = "";
            if (r >= 55296 && 56319 >= r) {
                if (t.length <= e + 1) {
                    throw "High surrogate without following low surrogate";
                }
                if (n = t.charCodeAt(e + 1), 56320 > n || n > 57343) {
                    throw "High surrogate without following low surrogate";
                }
                return t.charAt(e) + t.charAt(e + 1)
            }
            if (r >= 56320 && 57343 >= r) {
                if (0 === e) {
                    throw "Low surrogate without preceding high surrogate";
                }
                if (o = t.charCodeAt(e - 1), 55296 > o || o > 56319) {
                    throw "Low surrogate without preceding high surrogate";
                }
                return !1
            }
            return t.charAt(e)
        };
        for (r = 0, o = 0; r < e.length; r++) (n = i(e, r)) !== !1 && o++;
        return o
    },

    sleep(milliseconds) {
        let start = new Date().getTime();
        for (let i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    },

    time() {
        return new Date().getTime();
    },

    trim(t, e) {
        let r, n = 0,
            o = 0;
        for (t += "", e ? (e += "", r = e.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, "$1")) : r = " \n\r	\f            ​\u2028\u2029　", n = t.length, o = 0; n > o; o++)
            if (-1 === r.indexOf(t.charAt(o))) {
                t = t.substring(o);
                break
            }
        for (n = t.length, o = n - 1; o >= 0; o--)
            if (-1 === r.indexOf(t.charAt(o))) {
                t = t.substring(0, o + 1);
                break
            }
        return -1 === r.indexOf(t.charAt(0)) ? t : ""
    },

    ucfirst(t) {
        t += "";
        let e = t.charAt(0).toUpperCase();
        return e + t.substr(1)
    },


    /**
     * @param needle
     * @param haystack
     * @param argStrict
     * @returns {*}
     */
    inArray(needle, haystack, argStrict) {
        let key = '';
        let strict = !!argStrict;
        if (strict) {
            for (key in haystack) {
                if (haystack[key] === needle) {
                    return key;
                }
            }
        } else {
            for (key in haystack) {
                if (haystack[key] == needle) {
                    return true;
                }
            }
        }
        return false
    },

    getClone(obj) {
        return JSON.parse(JSON.stringify(obj));
    },

    is(v) {
        return typeof v !== "undefined" || v !== null;
    },

    range(start, end) {
        let array = [];

        for (let i = start; i < end; i++) {
            array.push(i);
        }
        return array;
    },

    urldecode(str) {
        return decodeURIComponent((str + '')
            .replace(/%(?![\da-f]{2})/gi, function () {
                // PHP tolerates poorly formed escape sequences
                return '%25';
            })
            .replace(/\+/g, '%20'));
    },

    functionExists(fn) {
        return isFunction(window[fn]);
    },

    objectExists(fn) {
        return typeof (window[fn]) === 'object';
    },

    fetchInObject(obj, key) {
        let args = key.split('.');
        let argsLength = args.length;
        let c = 0;
        let t = obj;

        // Parse config data and fetch attribute
        while (typeof (t[args[c]]) !== 'undefined') {
            t = t[args[c]];
            ++c;
        }

        // If not found...
        if (c < argsLength) {
            //t = '';
            t = null;
        }

        return t;
    },

    objectMerge(obj1, obj2) {
        let obj3 = {};
        for (let attrName in obj1) {
            obj3[attrName] = obj1[attrName];
        }

        for (let attrName in obj2) {
            obj3[attrName] = obj2[attrName];
        }
        return obj3;
    },

    isset() {
        let a = arguments,
            l = a.length,
            i = 0,
            undef;

        if (l === 0) {
            throw new Error('Empty isset');
        }

        while (i !== l) {
            if (a[i] === undef || a[i] === null) {
                return false;
            }
            i++;
        }
        return true;
    },

    date(format, timestamp) {
        let jsdate, f;
        // Keep this here (works, but for code commented-out below for file size reasons)
        // let tal= [];
        let txtWords = [
            'Sun', 'Mon', 'Tues', 'Wednes', 'Thurs', 'Fri', 'Satur',
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        ];
        // trailing backslash -> (dropped)
        // a backslash followed by any character (including backslash) -> the character
        // empty string -> empty string
        let formatChr = /\\?(.?)/gi;
        let formatChrCb = function (t, s) {
            return f[t] ? f[t]() : s
        };
        let _pad = function (n, c) {
            n = String(n);
            while (n.length < c) {
                n = '0' + n
            }
            return n
        };
        f = {
            // Day
            d: function () {
                // Day of month w/leading 0; 01..31
                return _pad(f.j(), 2)
            },
            D: function () {
                // Shorthand day name; Mon...Sun
                return f.l()
                    .slice(0, 3)
            },
            j: function () {
                // Day of month; 1..31
                return jsdate.getDate()
            },
            l: function () {
                // Full day name; Monday...Sunday
                return txtWords[f.w()] + 'day'
            },
            N: function () {
                // ISO-8601 day of week; 1[Mon]..7[Sun]
                return f.w() || 7
            },
            S: function () {
                // Ordinal suffix for day of month; st, nd, rd, th
                let j = f.j();
                let i = j % 10;
                if (i <= 3 && parseInt((j % 100) / 10, 10) === 1) {
                    i = 0
                }
                return ['st', 'nd', 'rd'][i - 1] || 'th'
            },
            w: function () {
                // Day of week; 0[Sun]..6[Sat]
                return jsdate.getDay()
            },
            z: function () {
                // Day of year; 0..365
                let a = new Date(f.Y(), f.n() - 1, f.j());
                let b = new Date(f.Y(), 0, 1);
                return Math.round((a - b) / 864e5)
            },

            // Week
            W: function () {
                // ISO-8601 week number
                let a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3);
                let b = new Date(a.getFullYear(), 0, 4);
                return _pad(1 + Math.round((a - b) / 864e5 / 7), 2)
            },

            // Month
            F: function () {
                // Full month name; January...December
                return txtWords[6 + f.n()]
            },
            m: function () {
                // Month w/leading 0; 01...12
                return _pad(f.n(), 2)
            },
            M: function () {
                // Shorthand month name; Jan...Dec
                return f.F()
                    .slice(0, 3)
            },
            n: function () {
                // Month; 1...12
                return jsdate.getMonth() + 1
            },
            t: function () {
                // Days in month; 28...31
                return (new Date(f.Y(), f.n(), 0))
                    .getDate()
            },

            // Year
            L: function () {
                // Is leap year?; 0 or 1
                let j = f.Y()
                return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0
            },
            o: function () {
                // ISO-8601 year
                let n = f.n();
                let W = f.W();
                let Y = f.Y();
                return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0)
            },
            Y: function () {
                // Full year; e.g. 1980...2010
                return jsdate.getFullYear()
            },
            y: function () {
                // Last two digits of year; 00...99
                return f.Y()
                    .toString()
                    .slice(-2)
            },

            // Time
            a: function () {
                // am or pm
                return jsdate.getHours() > 11 ? 'pm' : 'am'
            },
            A: function () {
                // AM or PM
                return f.a().toUpperCase();
            },
            B: function () {
                // Swatch Internet time; 000..999
                let H = jsdate.getUTCHours() * 36e2;
                // Hours
                let i = jsdate.getUTCMinutes() * 60;
                // Minutes
                // Seconds
                let s = jsdate.getUTCSeconds();
                return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3)
            },
            g: function () {
                // 12-Hours; 1..12
                return f.G() % 12 || 12
            },
            G: function () {
                // 24-Hours; 0..23
                return jsdate.getHours();
            },
            h: function () {
                // 12-Hours w/leading 0; 01..12
                return _pad(f.g(), 2)
            },
            H: function () {
                // 24-Hours w/leading 0; 00..23
                return _pad(f.G(), 2)
            },
            i: function () {
                // Minutes w/leading 0; 00..59
                return _pad(jsdate.getMinutes(), 2)
            },
            s: function () {
                // Seconds w/leading 0; 00..59
                return _pad(jsdate.getSeconds(), 2)
            },
            u: function () {
                // Microseconds; 000000-999000
                return _pad(jsdate.getMilliseconds() * 1000, 6)
            },

            // Timezone
            e: function () {
                // Timezone identifier; e.g. Atlantic/Azores, ...
                // The following works, but requires inclusion of the very large
                // timezone_abbreviations_list() function.
                /*              return that.date_default_timezone_get();
                 */
                let msg = 'Not supported (see source code of date() for timezone on how to add support)'
                throw new Error(msg)
            },
            I: function () {
                // DST observed?; 0 or 1
                // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
                // If they are not equal, then DST is observed.
                let a = new Date(f.Y(), 0);
                // Jan 1
                let c = Date.UTC(f.Y(), 0);
                // Jan 1 UTC
                let b = new Date(f.Y(), 6);
                // Jul 1
                // Jul 1 UTC
                let d = Date.UTC(f.Y(), 6);
                return ((a - c) !== (b - d)) ? 1 : 0;
            },
            O: function () {
                // Difference to GMT in hour format; e.g. +0200
                let tzo = jsdate.getTimezoneOffset();
                let a = Math.abs(tzo);
                return (tzo > 0 ? '-' : '+') + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
            },
            P: function () {
                // Difference to GMT w/colon; e.g. +02:00
                let O = f.O();
                return (O.substr(0, 3) + ':' + O.substr(3, 2));
            },
            T: function () {
                // The following works, but requires inclusion of the very
                // large timezone_abbreviations_list() function.
                return 'UTC';
            },
            Z: function () {
                // Timezone offset in seconds (-43200...50400)
                return -jsdate.getTimezoneOffset() * 60;
            },

            // Full Date/Time
            c: function () {
                // ISO-8601 date.
                return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb)
            },
            r: function () {
                // RFC 2822
                return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb)
            },
            U: function () {
                // Seconds since UNIX epoch
                return jsdate / 1000 | 0;
            }
        };

        let _date = function (format, timestamp) {
            jsdate = (timestamp === undefined ? new Date() // Not provided
                    : (timestamp instanceof Date) ? new Date(timestamp) // JS Date()
                        : new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
            );
            return format.replace(formatChr, formatChrCb)
        };

        return _date(format, timestamp)
    },

    throttle(fn, threshhold, scope) {
        threshhold || (threshhold = 250);
        let last,
            deferTimer;
        return function () {
            let context = scope || this;

            let now = +new Date,
                args = arguments;
            if (last && now < last + threshhold) {
                // hold on to it
                clearTimeout(deferTimer);
                deferTimer = setTimeout(function () {
                    last = now;
                    fn.apply(context, args);
                }, threshhold);
            } else {
                last = now;
                fn.apply(context, args);
            }
        };
    },

    debounce(fn, delay) {
        let timer = null;
        return function () {
            let context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                fn.apply(context, args);
            }, delay);
        };
    },

    stripTags(input, allowed) {
        if (input === null) {
            input = '';
        }
        // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
        allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('')

        let tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
        let commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;

        return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : ''
        })
    },

    stackTrace() {
        var err = new Error();
        return err.stack;
    },

    replaceAll(target, search, replacement) {
        return target.replace(new RegExp(search, 'g'), replacement);
    },
};