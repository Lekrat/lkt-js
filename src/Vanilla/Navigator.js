export const Navigator = {
    data: {
        supportsLocalStorage: 0,
    },
    supportsPromises () {
        return typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1;
    },
    supportsLocalStorage (){
        if (this.data.supportsLocalStorage === 0){
            let supportsLocalStorage = 1;
            if (typeof localStorage === 'object') {
                try {
                    localStorage.setItem('localStorage', 1);
                    localStorage.removeItem('localStorage');
                } catch (e) {
                    supportsLocalStorage = -1;
                }
            } else {
                supportsLocalStorage = -1;
            }
            this.data.supportsLocalStorage = supportsLocalStorage;
        }
        return this.data.supportsLocalStorage === 1;
    },
    supportsRgba: function () {
        let scriptElement = document.getElementsByTagName('script')[0],
            prevColor = scriptElement.style.color,
            r = true;
        try {
            scriptElement.style.color = 'rgba(1,5,13,0.44)';
            scriptElement.style.color = prevColor;
        } catch (e) {
            r = false;
        }
        return r;
    },
    isAndroid () {
        return /(android)/i.test(navigator.userAgent);
    },
    isAndroid5 () {
        return /(android 5)/i.test(navigator.userAgent);
    },
    isSafari () {
        return /(safari)/i.test(navigator.userAgent) && !this.isChrome();
    },
    isChrome () {
        return /(chrome)/i.test(navigator.userAgent);
    },
    isIPhone () {
        return /(iPhone|iPod)/i.test(navigator.userAgent);
    },
    isIPad () {
        return /(iPad)/i.test(navigator.userAgent);
    },
    isMobile () {
        return /(mobile|iPhone|iPod)/i.test(navigator.userAgent);
    }
};
