export const SafeType = {

    isFunction(fn) {
        return typeof (fn) === 'function';
    },

    isObject(mixedVar) {
        return typeof (mixedVar) === 'object';
    },

    isUndefined(mixedVar) {
        return typeof (mixedVar) === 'undefined';
    },
};