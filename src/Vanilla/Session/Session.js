import {Cookie} from "./Cookie";
import {Storage} from "./Storage";

export const Session = {
    Cookie: Cookie,
    Storage: Storage,

    set(name, value, expires) {
        if (Lkt.Navigator.supportsLocalStorage() === true) {
            return this.Storage.set(name, value, expires);
        }
        return this.Cookie.set(name, value, expires);
    },

    get(cname) {
        if (Lkt.Navigator.supportsLocalStorage() === true) {
            return this.Storage.get(cname);
        }
        return this.Cookie.get(cname);
    },

    del(name) {
        if (Lkt.Navigator.supportsLocalStorage() === true) {
            return this.Storage.del(name);
        }
        return this.Cookie.del(name);
    }
};
