export const Storage = {
    set(name, value, expires) {
        window.localStorage.setItem(name, value);
        return true;
    },

    get(cname) {
        return window.localStorage.getItem(cname);
    },

    del(name) {
        return window.localStorage.removeItem(name);
    }
};
