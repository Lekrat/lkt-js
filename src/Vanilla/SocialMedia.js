export class SocialMedia {
    static check(value){
        if (SocialMedia.isFacebook(value)){
            return 'facebook';
        }

        if (SocialMedia.isTwitter(value)){
            return 'twitter';
        }

        if (SocialMedia.isLinkedIn(value)){
            return 'linkedIn';
        }

        if (SocialMedia.isUrl(value)){
            return 'url';
        }

        return '';
    }

    static isFacebook(value){
        return new RegExp('(http(s?)://)*(?:www\\.)?.*?facebook\\.com').test(value);
    }

    static isTwitter(value){
        return new RegExp('(http(s?)://)*(?:www\\.)?.*?twitter\\.com').test(value);
    }

    static isLinkedIn(value){
        return new RegExp('(http(s?)://)*(?:www\\.)?.*?linkedin\\.com').test(value);
    }

    static isUrl(value){
        return new RegExp('^(https?|chrome):\\/\\/[^\\s$.?#].[^\\s]*$').test(value);
    }
}
