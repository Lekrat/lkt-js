export const EventManagement = {
    methods: {
        propagate: function (event) {
            // console.log('Emited event', data);
            this.$emit(event.name, event);
        },
        capture: function (event) {
            // console.log('Captured event', data);
            let cbName = event.name.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });
            if (typeof this[cbName] === 'function'){
                this[cbName](event);
            }
        }
    }
};
