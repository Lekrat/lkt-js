export const Helper = {
    methods: {
        objectMerge: function (obj1, obj2) {
            for (var key in obj2) {
                if (obj2.hasOwnProperty(key)) {
                    obj1[key] = obj2[key];
                }
            }
            return obj1;
        },
        objectMergeSafe: function (obj1, obj2) {
            for (var key in obj2) {
                if (obj1.hasOwnProperty(key) && obj2.hasOwnProperty(key)) {
                    obj1[key] = obj2[key];
                }
            }
            return obj1;
        },
        fillString: function (str, replacements) {
            for (var k in replacements) {
                if (replacements.hasOwnProperty(k)) {
                    str = str.replace(':' + k, replacements[k]);
                }
            }
            return str;
        },
        scrollTop: function () {
            if (!window.scrollTopTimeout){
                window.scrollTopTimeout = undefined;
            }
            if (document.body.scrollTop !== 0 || document.documentElement.scrollTop !== 0){
                document.body.scrollBy(0, -50);
                window.scrollTopTimeout = setTimeout(this.scrollTop, 10);
            } else {
                clearTimeout(window.scrollTopTimeout);
            }
        },
    },
};
