export const Item = {
    props: {
        data: {
            type: Object,
            default () {
                return {};
            },
        }
    },
    data: function () {
        return {
            lkt: {}
        };
    },
    methods: {
        parseData: function (data) {
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    this.lkt[key] = data[key];
                }
            }
        },
        init: function () {
            this.parseData(this.$props.data);

            if (this.afterInit) {
                this.afterInit();
            }
        }
    },
    watch: {
        data: function () {
            if (this.init) {
                this.init();
            }
        }
    },
    created: function() {
        if (this.init) {
            this.init();
        }
    }
};
