export const StateCommon = {
    computed: {
        appState(){
            return this.$root.state;
        },
    },
    methods: {
        getState(key = ''){
            return this.$root.state[key];
        },
        setState(key = '', val = undefined){
            this.$root.$set(this.$root.state, key, val);
        }
    },
};
