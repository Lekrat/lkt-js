export const StateRoot = {
    data(){
        return {
            state: {}
        }
    },

    watch: {
        state: {
            handler(){},
            deep: true
        }
    }
};
